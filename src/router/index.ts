import { createRouter, createWebHistory } from 'vue-router'
import JavaScriptView from '@/views/JavaScriptView.vue';
import JavaScriptTasks from '@/views/JavaScriptTasks.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'js',
      component: JavaScriptView
    },
    {
      path: '/js-tasks',
      name: 'js-tasks',
      component: JavaScriptTasks
    },
  ]
})

export default router
